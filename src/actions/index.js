import axios from 'axios';

import { GOOGLE_MAPS_JAVASCRIPT_API_KEY } from '../_config';
import {
    CONFIRM_ADDRESS
} from './types';

const OUTPUT_FORMAT = 'json';
const GOOGLE_MAPS_GEOCODING_BASE_URL = `https://maps.googleapis.com/maps/api/geocode/${OUTPUT_FORMAT}`;

function parseAddress(addressObject) {
    let address = '';
    address += addressObject.streetOne.replace(/ /g,"+");
    address += ',+';
    address += addressObject.city.replace(/ /g,"+");
    address += ',+';
    address += addressObject.state.replace(/ /g,"+");
    address += ',+';
    address += addressObject.zipcode.replace(/ /g,"+");

    return address;
}

/*
    confirms an Address through Google Maps Geocoding API and returns coordinates.

    Args:
        addressObject: Object that contains:
        - streetOne: String
        - streetTwo: String
        - city: String
        - state: String
        - zipcode: String
        - phoneNumber: String
*/
export function confirmAddress(addressObject) {
    const parsedAddress = parseAddress(addressObject);
    const url = `${GOOGLE_MAPS_GEOCODING_BASE_URL}?address=${parsedAddress}&key=${GOOGLE_MAPS_JAVASCRIPT_API_KEY}`;
    const request = axios.get(url);

    return {
        type: CONFIRM_ADDRESS,
        payload: request
    };
}

/*
    submits an Address to the backend.
*/
// export function submitAddress(addressObject) {
//
// }
