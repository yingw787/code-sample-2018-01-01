// Confirming an address sends the address to Google Maps API
// and updates the map and adds a marker for the shop.
export const CONFIRM_ADDRESS = 'confirm_address';

// Submitting an address submits the address as a JSON PATCH request to the backend
// Also provides a location to add in other webhooks, such as marketing analytics
export const SUBMIT_ADDRESS = 'submit_address';
