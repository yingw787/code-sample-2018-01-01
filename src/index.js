import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route } from 'react-router';
import createBrowserHistory from 'history/createBrowserHistory';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxPromise from 'redux-promise';

import reducers from './reducers';
import App from './components/App';

const createStoreFromMiddleware = applyMiddleware(ReduxPromise)(createStore);

ReactDOM.render(
    <Provider store={createStoreFromMiddleware(reducers)}>
        <Router history={ createBrowserHistory() }>
            <Route path="/" component={App}>
            </Route>
        </Router>
    </Provider>
    , document.querySelector('.app-container')
);
