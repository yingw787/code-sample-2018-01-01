import { CONFIRM_ADDRESS } from '../actions/types';

export default function(state=[], action) {
    switch (action.type) {
    case CONFIRM_ADDRESS:
        return [action.payload.data, ...state];
    }
    return state;
}
