import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import MapsReducer from './reducer_maps';

const rootReducer = combineReducers({
    form: formReducer,
    mapData: MapsReducer
});

export default rootReducer;
