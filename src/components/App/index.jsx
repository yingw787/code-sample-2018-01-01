import React, { Component } from 'react';

import AppBar from '../common/AppBar';
import QuestionOne from '../questions/1';
import styles from './styles.scss';

const links = [
    {
        'name': 'Contact',
        'href': '#'
    },
    {
        'name': 'Login',
        'href': '#'
    }
];

export default class App extends Component {
    render() {
        return (
            <div className={styles.container}>
                <AppBar
                    links={links}
                />
                <QuestionOne />
            </div>
        );
    }
}
