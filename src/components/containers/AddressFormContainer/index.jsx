import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import classnames from 'classnames';

import { confirmAddress } from '../../../actions';
import styles from './styles.scss';
import AddressForm from '../../forms/AddressForm';

const formTitle = "Company Address";
const formDescription = "We pride ourselves on keeping manufacturing in the United States.\nPut your shop on the map!";

const formElements = {
    "streetOne": {
        "key": "streetOne",
        "text": "Street Address, PO Box",
        "errors": {
            "isEmpty": "Please provide a street address."
        }
    },
    "streetTwo": {
        "key": "streetTwo",
        "text": "Apartment, Suite, Unit, Building, Floor, etc.",
        "textOptional": "(optional)"
    },
    "city": {
        "key": "city",
        "text": "City",
        "errors": {
            "isEmpty": "Please provide a city."
        }
    },
    "state": {
        "key": "state",
        "text": "State",
        "errors": {
            "isEmpty": "Please provide a state."
        }
    },
    "zipcode": {
        "key": "zipcode",
        "text": "Zip Code",
        "errors": {
            "isEmpty": "Please provide a zip code.",
            "invalid": "Invalid zip code."
        }
    },
    "phoneNumber": {
        "key": "phoneNumber",
        "text": "Phone Number",
        "errors": {
            "isEmpty": "Please provide a phone number.",
            "invalid": "Invalid phone number."
        }
    },
    "submit": {
        "validate": "Confirm",
        "confirm": "Next"
    }
};

const formValidation = {
    "failure": "We could not find your address through Google Maps.",
    "success": "We found you!"
};


class AddressFormContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            address: {},
            validated: {
                attempted: false,
                isSuccessful: false
            }
        };
    }

    generateClasses(tag) {
        switch(tag) {
        case 'success-text':
            return classnames({
                [styles.verticalAlignText]: true,
                [styles.successText]: true
            });
        case 'failure-text':
            return classnames({
                [styles.verticalAlignText]: true,
                [styles.errorText]: true
            });
        default:
            throw new Error('unknown tag in generateClasses() in AddressFormContainer.');
        }
    }

    componentWillReceiveProps(nextProps) {
        // You don't have to do this check first, but it can help prevent an unneeded render
        let validated = this.state.validated;
        validated.attempted = true;
        if (nextProps.mapData !== this.state.mapData && nextProps.mapData[0].status == 'OK') {
            validated.isSuccessful = true;
            this.setState({
                validated: validated
            });
        }
        else {
            validated.isSuccessful = false;
            this.setState({
                validated: validated
            });
        }
    }

    submit = (address) => {
        if (address == this.state.address && this.state.validated.isSuccessful) {
            console.log('PATCH request', address);
        }
        else {
            let validated = this.state.validated;
            validated.attempted = false;
            this.setState({
                validated: validated,
                address: address
            });

            // Confirm address through Google Maps Geocoding API
            this.props.confirmAddress(address);
        }
    }

    render() {
        return (
            <div>
                <div>
                    <h2>{formTitle}</h2>
                    <h4 className={styles.form_description}>{formDescription}</h4>
                </div>
                <br />
                <AddressForm
                    onSubmit={this.submit}
                    formElements={formElements}
                    validated={this.state.validated.isSuccessful}
                />
                {
                    this.state.validated.attempted && this.state.validated.isSuccessful &&
                    (
                        <div className={this.generateClasses('success-text')}>{formValidation.success}</div>
                    )
                    || this.state.validated.attempted && !this.state.validated.isSuccessful &&
                    (
                        <div className={this.generateClasses('failure-text')}>{formValidation.failure}</div>
                    )
                }
            </div>
        );
    }
}

function mapStateToProps({ mapData }) {
    return {
        mapData
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ confirmAddress }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AddressFormContainer);

AddressFormContainer.propTypes = {
    confirmAddress: PropTypes.func.isRequired
};
