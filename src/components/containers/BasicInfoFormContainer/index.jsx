import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import classnames from 'classnames';

import { confirmAddress } from '../../../actions';
import styles from './styles.scss';
import BasicInfoForm from '../../forms/BasicInfoForm';

const formTitle = "Partner Information";
const formDescription = "Welcome! We are excited to have you join our partner network. To get started, please fill out your company information and login credentials.";

const formElements = {
    "firstName": {
        "key": "firstName",
        "text": "First Name",
        "errors": {
            "isEmpty": "Please provide a first name."
        }
    },
    "lastName": {
        "key": "lastName",
        "text": "Last Name",
        "errors": {
            "isEmpty": "Please provide a last name."
        }
    },
    "company": {
        "key": "company",
        "text": "Company Name",
        "errors": {
            "isEmpty": "Please provide a company."
        }
    },
    "shopTypes": {
        "key": "shopTypes",
        "text": "What kind of shop are you?",
        "types": {
            "machining": {
                "key": "machining",
                "text": "Machining"
            },
            "sheetMetal": {
                "key": "sheetMetal",
                "text": "Sheet Metal"
            },
            "additive": {
                "key": "additive",
                "text": "Additive"
            },
            "casting": {
                "key": "casting",
                "text": "Casting"
            },
            "finishes": {
                "key": "finishes",
                "text": "Finishes"
            }
        },
        "errors": {
            "isEmpty": "Please choose at least one shop type."
        }
    },
    "email": {
        "key": "email",
        "text": "Email",
        "textOptional": "(login username)",
        "errors": {
            "isEmpty": "Please provide an email address.",
            "invalid": "Invalid email address."
        },
    },
    "password": {
        "key": "password",
        "text": "Password",
        "hyperlinks": {
            "showRules": "show rules",
            "showPassword": "Show Password"
        },
        "errors": {
            "isEmpty": "Please enter a password.",
            "invalid": "Invalid password."
        }
    },
    "tos": {
        "key": "tos",
        "text": "I reviewed and agree to Xometry's Terms of Service and Privacy Policy.",
        "links": {
            "tos": "#",
            "privacyPolicy": "#"
        },
        "errors": {
            "invalid": "You need to agree to the Terms of Service and Privacy Policy."
        }
    },
    "submit": {
        "confirm": "Next"
    }
};

// generateClasses(tag) {
//     switch(tag) {
//     case 'success-text':
//         return classnames({
//             [styles.verticalAlignText]: true,
//             [styles.successText]: true
//         });
//     case 'failure-text':
//         return classnames({
//             [styles.verticalAlignText]: true,
//             [styles.errorText]: true
//         });
//     default:
//         throw new Error('unknown tag in generateClasses() in AddressFormContainer.');
//     }
// }

// componentWillReceiveProps(nextProps) {
//     // You don't have to do this check first, but it can help prevent an unneeded render
//     let validated = this.state.validated;
//     validated.attempted = true;
//     if (nextProps.mapData !== this.state.mapData && nextProps.mapData[0].status == 'OK') {
//         validated.isSuccessful = true;
//         this.setState({
//             validated: validated
//         });
//     }
//     else {
//         validated.isSuccessful = false;
//         this.setState({
//             validated: validated
//         });
//     }
// }

// if (address == this.state.address && this.state.validated.isSuccessful) {
//     console.log('PATCH request', address);
// }
// else {
//     let validated = this.state.validated;
//     validated.attempted = false;
//     this.setState({
//         validated: validated,
//         address: address
//     });
//
//     // Confirm address through Google Maps Geocoding API
//     this.props.confirmAddress(address);
// }

export default class BasicInfoFormContainer extends Component {
    constructor(props) {
        super(props);
    }

    submit = (address) => {
        console.log(address);
    }

    render() {
        return (
            <div>
                <div>
                    <h2>{formTitle}</h2>
                    <h4 className={styles.form_description}>{formDescription}</h4>
                </div>
                <br />
                <BasicInfoForm
                    onSubmit={this.submit}
                    formElements={formElements}
                />
            </div>
        );
    }
}

// {
//     this.state.validated.attempted && this.state.validated.isSuccessful &&
//     (
//         <div className={this.generateClasses('success-text')}>{formValidation.success}</div>
//     )
//     || this.state.validated.attempted && !this.state.validated.isSuccessful &&
//     (
//         <div className={this.generateClasses('failure-text')}>{formValidation.failure}</div>
//     )
// }

// function mapStateToProps({ mapData }) {
//     return {
//         mapData
//     };
// }
//
// function mapDispatchToProps(dispatch) {
//     return bindActionCreators({ confirmAddress }, dispatch);
// }
//
// export default connect(mapStateToProps, mapDispatchToProps)(AddressFormContainer);
//
// AddressFormContainer.propTypes = {
//     confirmAddress: PropTypes.func.isRequired
// };
