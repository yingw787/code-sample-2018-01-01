import React, { Component } from 'react';
import classnames from 'classnames';

import styles from './styles.scss';
import AddressFormContainer from '../../containers/AddressFormContainer';
import UnitedStatesMap from '../../common/UnitedStatesMap';

export default class QuestionTwo extends Component {
    constructor(props) {
        super(props);
        this.generateContainerClasses = this.generateContainerClasses.bind(this);
    }

    generateContainerClasses(id) {
        switch(id) {
        case 'top-level-container':
            return classnames({
                [styles.address_form_container]: true,
                'container-fluid': true
            });
        case 'row':
            return classnames({
                'row': true
            });
        case 'col-form':
            return classnames({
                [styles.address_form_sub_container]: true,
                'col-12': true,
                'col-lg-6': true,
                'col': true,
                'align-self-start': true
            });
        case 'col-map':
            return classnames({
                [styles.address_form_sub_container]: true,
                'col-12': true,
                'col-lg-6': true,
                'hidden-md-down': true,
                'col': true,
                'align-self-end': true
            });
        }
    }

    render() {
        return (
            <div className={this.generateContainerClasses('top-level-container')}>
                <div className={this.generateContainerClasses('row')}>
                    <div className={this.generateContainerClasses('col-form')}>
                        <div
                            id="address-form-container">
                            <AddressFormContainer />
                        </div>
                    </div>
                    <div className={this.generateContainerClasses('col-map')}>
                        <div
                            id="us-map">
                            <UnitedStatesMap />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
