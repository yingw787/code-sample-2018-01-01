import React, { Component } from 'react';

import styles from './styles.scss';

export default class GoogleMapMarker extends Component {
    render() {
        return (
            <div className={styles.google_maps_marker} />
        );
    }
}
