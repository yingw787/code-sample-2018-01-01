import React, { Component } from 'react';

import GoogleMap from './GoogleMap';
// import styles from './styles.scss';

export default class UnitedStatesMap extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <GoogleMap />
            </div>
        );
    }
}
