import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import { connect } from 'react-redux';

import GoogleMapMarker from './GoogleMapMarker';
import { mapStyles } from './mapStyles';
import { GOOGLE_MAPS_JAVASCRIPT_API_KEY } from '../../../_config';

const GEOGRAPHIC_US_CENTER = { lat: 39.50, lng: -98.35 };
const DEFAULT_ZOOM = 3;
// const FOCUS_ZOOM = 17; // Zoom on a street level
const FOCUS_ZOOM = 6;

class GoogleMap extends Component {
    static defaultProps = {
        center: GEOGRAPHIC_US_CENTER,
        zoom: DEFAULT_ZOOM
    };

    constructor(props) {
        super(props);
        this.generateGoogleMapMarker = this.generateGoogleMapMarker.bind(this);
        this.createMapOptions = this.createMapOptions.bind(this);
        this.state = {
            focusedCenter: GEOGRAPHIC_US_CENTER,
            focusedZoom: DEFAULT_ZOOM
        };
    }

    createMapOptions() {
        return {
            styles: mapStyles
        };
    }

    componentWillReceiveProps(nextProps) {
        // You don't have to do this check first, but it can help prevent an unneeded render
        if (nextProps.mapData !== this.state.mapData) {
            if (nextProps.mapData[0].status == 'OK') {
                this.setState({
                    focusedCenter: {
                        lat: nextProps.mapData[0].results[0].geometry.location.lat,
                        lng: nextProps.mapData[0].results[0].geometry.location.lng
                    },
                    // focusedZoom: FOCUS_ZOOM
                    focusedZoom: FOCUS_ZOOM
                });
            }
        }
    }

    generateGoogleMapMarker(mapData) {
        if (mapData[0] && mapData[0].status == 'OK') {
            // Right now assume only one address location
            const lat = mapData[0].results[0].geometry.location.lat;
            const lng = mapData[0].results[0].geometry.location.lng;

            return (
                <GoogleMapMarker
                    lat={lat}
                    lng={lng}
                />
            );
        }
        else {
            return null;
        }
    }

    render() {
        return (
            <div>
                <GoogleMapReact
                    bootstrapURLKeys={{
                        key: GOOGLE_MAPS_JAVASCRIPT_API_KEY
                    }}
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}
                    center={this.state.focusedCenter}
                    zoom={this.state.focusedZoom}
                    options={this.createMapOptions()}>
                    {this.generateGoogleMapMarker(this.props.mapData)}
                </GoogleMapReact>
            </div>
        );
    }
}

function mapStateToProps({ mapData }) {
    return {
        mapData
    };
}

export default connect(mapStateToProps)(GoogleMap);
