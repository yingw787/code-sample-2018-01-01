import React from 'react';
import classnames from 'classnames';

import styles from './styles.scss';

const generateClasses = (tag) => {
    switch(tag) {
    case 'select-field':
        return classnames({
            [styles.formInputSelect]: true,
            [styles.formInputLong]: true,
        });
    case 'error-text':
        return classnames({
            [styles.errorText]: true
        });
    case 'warning-text':
        return classnames({
            [styles.warningText]: true
        });
    default:
        throw new Error("Unrecognized tag in generateClasses in SelectFormField.");
    }
};

const generateSelectField = (input, dataFunc) => {
    return (
        <select
            {...input}
            className={generateClasses('select-field')}>
            {dataFunc()}
        </select>
    );
};

export const SelectFormField = ({ input, label, meta: { touched, error, warning }, ...custom }) => (
    <div>
        <span
            className={styles.fieldTitle}>
            {label}
        </span>
        {
            custom.optionalText &&
            <span
                className={styles.fieldTitleLight}>
                {custom.optionalText}
            </span>
        }
        <div>
            {generateSelectField(input, custom.dataFunc)}
            {
                touched &&
                (
                    (error && <span className={generateClasses('error-text')}>{error}</span>) ||
                    (warning && <span className={generateClasses('warning-text')}>{warning}</span>)
                )
            }
        </div>
    </div>
);
