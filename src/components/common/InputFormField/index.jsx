import React from 'react';
import classnames from 'classnames';

import styles from './styles.scss';

const generateClasses = (tag) => {
    switch(tag) {
    case 'input-field':
        return classnames({
            [styles.formInput]: true,
            [styles.formInputLong]: true
        });
    case 'error-text':
        return classnames({
            [styles.errorText]: true
        });
    case 'warning-text':
        return classnames({
            [styles.warningText]: true
        });
    default:
        throw new Error("Unrecognized tag in generateClasses in InputFormField.");
    }
};

const generateInputField = (input, type) => {
    return (
        <input
            {...input}
            type={type}
            className={generateClasses('input-field')}
        />
    );
};

export const InputFormField = ({ input, label, type, meta: { touched, error, warning }, ...custom }) => (
    <div>
        <span
            className={styles.fieldTitle}>
            {label}
        </span>
        {
            custom.optionalText &&
            <span
                className={styles.fieldTitleLight}>
                {custom.optionalText}
            </span>
        }
        <div>
            {generateInputField(input, type)}
            {
                touched &&
                (
                    (error && <span className={generateClasses('error-text')}>{error}</span>) ||
                    (warning && <span className={generateClasses('warning-text')}>{warning}</span>)
                )
            }
        </div>
    </div>
);
