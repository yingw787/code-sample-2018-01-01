import React from 'react';
import classnames from 'classnames';

import styles from './styles.scss';

const generateClasses = (tag) => {
    switch(tag) {
    case 'checkbox-wrapper':
        return classnames({
            'col': true,
            'col-12': true,
            'col-xs-auto': true,
            'col-sm-auto': true,
            'col-md-auto': true,
            'col-lg-auto': true
        });
    case 'checkbox':
        return classnames({
            'checkbox': true,
            [styles.checkbox]: true
        });
    case 'checkbox-label':
        return classnames({
            'form-check-label': true,
            [styles.checkboxLabel]: true
        })
    case 'checkbox-text':
        return classnames({
            [styles.checkboxText]: true
        })
    case 'error-text':
        return classnames({
            [styles.errorText]: true
        });
    case 'warning-text':
        return classnames({
            [styles.warningText]: true
        });
    default:
        throw new Error("Unrecognized tag in generateClasses in InputFormField.");
    }
};

const generateCheckbox = (input, type, label) => {
    return (
        <div class="form-check form-check-inline">
            <label class={generateClasses('checkbox-label')}>
                <input
                    {...input}
                    class="form-check-input"
                    type={type}
                    id={label}
                    value={label}
                />
                <div className={generateClasses('checkbox-text')}>
                    {label}
                </div>
            </label>
        </div>
    );
};

export const CheckboxFormField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div>
        <div className={generateClasses('checkbox-wrapper')}>
            {generateCheckbox(input, type, label)}
        </div>
    </div>
);
