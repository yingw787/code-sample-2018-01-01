import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import _ from 'lodash';

import styles from './styles.scss';
import logo from 'xometry_styles/logos/xometry-logo-white.svg';

export default class AppBar extends Component {
    constructor(props) {
        super(props);
        this.generateNavBarClasses = this.generateNavBarClasses.bind(this);
        this.generateNavBarLinks = this.generateNavBarLinks.bind(this);
        this.generateNavBarLinkClasses = this.generateNavBarLinkClasses.bind(this);
    }

    generateNavBarClasses() {
        return classnames({
            'navbar': true,
            'navbar-toggleable-sm': true,
            'navbar-light': true,
            'bg-faded': true,
            [styles.xometry_app_bar]: true
        });
    }

    generateNavBarLinkClasses(tag) {
        switch(tag) {
        case 'li':
            return classnames({
                'navbar-item': true,
                'active': false,
                'disabled': false,
                [styles.xometry_nav_bar_item_li]: true
            });
        case 'a':
            return classnames({
                'nav-link': true,
                [styles.xometry_nav_bar_item_a]: true
            });
        }
    }

    generateNavBarLinks() {
        return _.map(this.props.links, link => {
            return (
                <li
                    className={this.generateNavBarLinkClasses('li')}
                    key={link.name}>
                    <a className={this.generateNavBarLinkClasses('a')} href={link.href}>{link.name}</a>
                </li>
            );
        });
    }

    render() {
        return (
            <nav className={this.generateNavBarClasses()}>
                <button
                    className="navbar-toggler navbar-toggler-right"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="true"
                    aria-label="Toggle navigation">
                    <span
                        className="navbar-toggler-icon">
                    </span>
                </button>
                <a className="navbar-brand" href="https://xometry.com">
                    <img
                        className={styles.xometry_logo}
                        src={logo}
                    />
                </a>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ml-auto">
                        {this.generateNavBarLinks()}
                    </ul>
                </div>
            </nav>
        );
    }
}

AppBar.propTypes = {
    links: PropTypes.array.isRequired
};
