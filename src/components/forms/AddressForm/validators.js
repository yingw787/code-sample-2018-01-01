/*
    Validation functions.
*/

// Require value be truthy. Print errorText if not.
export const required = errorText => value =>
    value
    ? undefined
    : errorText;

// Check zipcode (US only). Print errorText if not valid.
export const zipcode = errorText => value =>
    value && /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(value)
        ? undefined
        : errorText;

/*
    Check phone number (US only). Print errorText if not valid.

    Valid formats:
    - (123) 456-7890
    - 123-456-7890
    - 123.456.7890
    - 1234567890
    - +31636363634
    - 075-63546725
*/
export const phoneNumber = errorText => value =>
    value && /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(value)
        ? undefined
        : errorText;
