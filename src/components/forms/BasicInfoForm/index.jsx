import React, { PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form';
import classnames from 'classnames';
import _ from 'lodash';

import { InputFormField } from '../../common/InputFormField';
import { SelectFormField } from '../../common/SelectFormField';
import { CheckboxFormField } from '../../common/CheckboxFormField';
// import { confirmAddress } from '../../../actions';
import { generateStateFields } from '../../../data/utils';
import { required, zipcode, phoneNumber } from './validators';
import { normalizePhone } from './normalizers';
import styles from './styles.scss';

const generateClasses = (tag) => {
    switch(tag) {
    case 'submit-button-container':
        return classnames({
            [styles.submit_button_container]: true,
            'col-12': true
        });
    case 'submit-button':
        return classnames({
            'btn': true,
            [styles.submit_button]: true
        });
    default:
        throw new Error("Unrecognized tag in generateClasses() in AddressForm.");
    }
};

const generateCheckboxes = (items) => {
    return _.map(items, (item) => {
        return (
            <Field
                key={item.key}
                name={item.key}
                type={'checkbox'}
                label={item.text}
                component={CheckboxFormField}
            />
        );
    });
}

const BasicInfoForm = (props) => {
    const { handleSubmit, formElements, submitting, validated } = props;
    return (
        <form onSubmit={handleSubmit}>
            <div className="row">
                <div className="col-12 col-sm-6 col-md-6 col-lg-6 form-group">
                    <Field
                        name={formElements.firstName.key}
                        label={formElements.firstName.text}
                        component={InputFormField}
                        validate={[required(formElements.firstName.errors.isEmpty)]}
                    />
                </div>
                <div className="col-12 col-sm-6 col-md-6 col-lg-6 form-group">
                    <Field
                        name={formElements.lastName.key}
                        label={formElements.lastName.text}
                        component={InputFormField}
                        validate={[required(formElements.lastName.errors.isEmpty)]}
                    />
                </div>
            </div>
            <div className="form-group">
                <Field
                    name={formElements.company.key}
                    label={formElements.company.text}
                    component={InputFormField}
                    validate={[required(formElements.company.errors.isEmpty)]}
                />
            </div>
            <div className="container-fluid">
                <div className="row">
                    {generateCheckboxes(formElements.shopTypes.types)}
                </div>
            </div>
            <br />
            <div className={generateClasses('submit-button-container')}>
                <button
                    type="submit"
                    disabled={submitting}
                    className={generateClasses('submit-button')}>
                    { formElements.submit.confirm }
                </button>
            </div>
        </form>
    );
};

export default reduxForm({
    form: 'basicInfo'
})(BasicInfoForm);

BasicInfoForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    formElements: PropTypes.object.isRequired
};




// <div className="form-group">
//     <Field
//         name={formElements.streetTwo.key}
//         label={formElements.streetTwo.text}
//         component={InputFormField}
//         optionalText={formElements.streetTwo.textOptional}
//     />
// </div>
// <div className="row">
//     <div className="col-12 col-sm-4 col-md-4 col-lg-4 form-group">
//         <Field
//             name={formElements.city.key}
//             label={formElements.city.text}
//             component={InputFormField}
//             validate={[required(formElements.city.errors.isEmpty)]}
//         />
//     </div>
//     <div className="col-12 col-sm-4 col-md-4 col-lg-4 form-group">
//         <Field
//             name={formElements.state.key}
//             label={formElements.state.text}
//             component={SelectFormField}
//             dataFunc={generateStateFields}
//             validate={[required(formElements.state.errors.isEmpty)]}
//         />
//     </div>
//     <div className="col-12 col-sm-4 col-md-4 col-lg-4 form-group">
//         <Field
//             name={formElements.zipcode.key}
//             label={formElements.zipcode.text}
//             component={InputFormField}
//             validate={[required(formElements.zipcode.errors.isEmpty), zipcode(formElements.zipcode.errors.invalid)]}
//         />
//     </div>
// </div>
// <div className="form-group">
//     <Field
//         name={formElements.phoneNumber.key}
//         label={formElements.phoneNumber.text}
//         component={InputFormField}
//         normalize={normalizePhone}
//         validate={[required(formElements.phoneNumber.errors.isEmpty), phoneNumber(formElements.phoneNumber.errors.invalid)]}
//     />
// </div>
//
