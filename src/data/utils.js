/*
    Utilities file in order to parse data found in the data folder.
*/
import React from 'react';
import _ from 'lodash';

import states from './states_hash.json';

// Generates the states for the state selection dropdown menu.
export const generateStateFields = () => {
    return _.map(Object.values(states), stateName => {
        return (
            <option key={stateName}>{stateName}</option>
        );
    });
};
