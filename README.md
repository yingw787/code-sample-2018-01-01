# Xometry Vendor Portal

A library of reusable React.js components for Xometry's single page applications.

## Installation

```bash
git clone https://github.com/yingw787/xometry_react_components /path/to/repo
cd /path/to/repo
npm run configure
```

In addition, you should copy a correct _config_ file and the x-logo.svg (Xometry logo) from somebody at the company, otherwise compilation will fail.


## Building and Running for Development
```bash
npm run dev
```

## Building for Development
```bash
npm run build-dev
```

## Building for Production
```bash
npm run build
```

## Making changes to the xometry_styles repository

```bash
npm run xometry_styles_create_updates_branch
git checkout subtree_xometry_styles
## Make desired changes to xometry_styles here
npm run git-pull
git add .
git commit -m "Add desired commit message here"
npm run git-push
git checkout master
npm run xometry_styles_delete_subtree
npm run xometry_styles_update_subtree
```
