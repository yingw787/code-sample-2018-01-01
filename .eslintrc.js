module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "node": true,
        "mocha": true,
        "es6": true
    },
    "globals": {
        "expect": true,
        "mount": true,
        "render": true,
        "shallow": true
    },
    "parser": "babel-eslint",
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        },
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "no-unused-vars": "warn",
        "react/jsx-uses-vars": "warn",
        "react/jsx-uses-react": "warn",
        "indent": [
            "warn",
            4
        ],
        "linebreak-style": [
            "warn",
            "unix"
        ],
        "semi": [
            "warn",
            "always"
        ],
        "no-trailing-spaces": "warn",
        "eol-last": "warn",
        "camelcase": "warn",
        "object-curly-spacing": ["warn", "always"],
        "no-console": "warn",
        "no-alert": "warn",
        "no-debugger": "warn",
    }
};
