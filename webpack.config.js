var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var FaviconsWebpackPlugin = require('favicons-webpack-plugin');

module.exports = {
    entry: {
        bundle: './src/index.js'
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['.js', '.jsx', '.scss'],
        alias: {
            'xometry_styles': path.resolve('./xometry_styles')
        }
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx|scss)$/,
                loader: 'source-map-loader',
                enforce: 'pre'
            },
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: path.resolve('node_modules'),
                query: {
                    presets: ['react', 'env', 'stage-0']
                }
            },
            {
                test: /\.(js|jsx)$/,
                loader: 'eslint-loader',
                exclude: path.resolve('node_modules'),
                enforce: 'pre'
            },
            {
                test: /\.(css|scss|sass)$/,
                use: [
                        'style-loader',
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                importLoaders: 1,
                                camelCase: true,
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                includePaths: [path.resolve('./xometry_styles')]
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins() {
                                    return [require('autoprefixer')];
                                }
                            }
                        }
                    ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 40000
                        }
                    },
                    'image-webpack-loader'
                ]
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/index.html'
        }),
        new FaviconsWebpackPlugin({
            logo: path.resolve('./x-logo.svg')
        })
    ]
}
